package lgtu.pi16.marking_machine.protocol;

/**
 * Типы телеграмм
 * @author vladimir
 *
 */
public enum TYPES {
	MD,
	MS;
}
