package lgtu.pi16.marking_machine.protocol;

/**
 * Пакет обмена данными с PLC
 */
public class Package {
	public byte[] data;
	public TYPES packageType;
}
