import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.SystemColor;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;
import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JEditorPane;
import java.awt.Canvas;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;
import java.awt.Label;
import java.awt.Choice;
import java.awt.ScrollPane;
import java.awt.Scrollbar;
import javax.swing.JTree;
import javax.swing.JPasswordField;
import javax.swing.JMenu;
import javax.swing.JCheckBoxMenuItem;

public class Model {

	private JFrame frame;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Model window = new Model();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Model() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setBounds(100, 100, 1337, 906);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("+");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			}
		});
		
		JButton button = new JButton("-");
		button.setToolTipText("");
		button.setFont(new Font("Tahoma", Font.PLAIN, 11));
		button.setBounds(56, 233, 46, 46);
		frame.getContentPane().add(button);
		btnNewButton.setToolTipText("");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnNewButton.setBounds(10, 233, 46, 46);
		frame.getContentPane().add(btnNewButton);
		
		
		
		Date dateNow = new Date();
    	SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd/MM/yyyy");
    	SimpleDateFormat formatForTimeNow = new SimpleDateFormat("hh:mm:ss");
    	// JLabel labeldate = new JLabel("���� " + formatForDateNow.format(dateNow));
    	// JLabel labeltime = new JLabel("����� " + formatForTimeNow.format(dateNow));
		
		
		
		
		
		JTextArea textArea = new JTextArea();
		textArea.setText("����: " + formatForDateNow.format(dateNow));
		textArea.setForeground(Color.BLACK);
		textArea.setFont(new Font("Times New Roman", Font.BOLD, 16));
		textArea.setEditable(false);
		textArea.setBackground(SystemColor.menu);
		textArea.setBounds(10, 11, 200, 23);
		frame.getContentPane().add(textArea);
		
		JTextArea textArea_1 = new JTextArea();
		textArea_1.setText("�����: " + formatForTimeNow.format(dateNow));
		textArea_1.setForeground(Color.BLACK);
		textArea_1.setFont(new Font("Times New Roman", Font.BOLD, 16));
		textArea_1.setEditable(false);
		textArea_1.setBackground(SystemColor.menu);
		textArea_1.setBounds(10, 45, 200, 23);
		frame.getContentPane().add(textArea_1);
		
		textField_1 = new JTextField();
		textField_1.setBackground(Color.WHITE);
		textField_1.setEditable(false);
		textField_1.setBounds(220, 14, 516, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBackground(Color.WHITE);
		textField_2.setEditable(false);
		textField_2.setBounds(746, 11, 575, 140);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBackground(Color.WHITE);
		textField_3.setEditable(false);
		textField_3.setBounds(220, 48, 516, 20);
		frame.getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBackground(Color.WHITE);
		textField_4.setEditable(false);
		textField_4.setBounds(220, 113, 86, 29);
		frame.getContentPane().add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBackground(Color.WHITE);
		textField_5.setEditable(false);
		textField_5.setBounds(338, 113, 65, 29);
		frame.getContentPane().add(textField_5);
		textField_5.setColumns(10);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("\u043F\u043E\u0440\u044F\u0434\u043E\u043A \u043B\u0438\u0441\u0442\u043E\u0432");
		chckbxNewCheckBox.setFont(new Font("Tahoma", Font.BOLD, 11));
		chckbxNewCheckBox.setBounds(409, 119, 128, 23);
		frame.getContentPane().add(chckbxNewCheckBox);
		
		JCheckBox chckbxNewCheckBox_1 = new JCheckBox("\u0428\u043B\u0438\u0444\u043E\u0432\u0430\u043B\u044C\u043D\u0430\u044F \u043F\u043B\u0430\u0441\u0442\u0438\u043D\u0430");
		chckbxNewCheckBox_1.setFont(new Font("Tahoma", Font.BOLD, 11));
		chckbxNewCheckBox_1.setBounds(539, 121, 171, 21);
		frame.getContentPane().add(chckbxNewCheckBox_1);
		
		JTextArea textArea_2 = new JTextArea();
		textArea_2.setText("\u043D\u043E\u043C\u0435\u0440 \u043E\u0431\u0440\u0430\u0437\u0446\u0430");
		textArea_2.setForeground(Color.BLACK);
		textArea_2.setFont(new Font("Tahoma", Font.BOLD, 11));
		textArea_2.setEditable(false);
		textArea_2.setBackground(SystemColor.menu);
		textArea_2.setBounds(220, 89, 99, 23);
		frame.getContentPane().add(textArea_2);
		
		Canvas canvas = new Canvas();
		canvas.setBackground(Color.GRAY);
		canvas.setBounds(0, 233, 1308, 611);
		frame.getContentPane().add(canvas);
		
		JTextArea txtrKlippetillaeg = new JTextArea();
		txtrKlippetillaeg.setText("Klippetillaeg");
		txtrKlippetillaeg.setForeground(Color.BLACK);
		txtrKlippetillaeg.setFont(new Font("Tahoma", Font.BOLD, 11));
		txtrKlippetillaeg.setEditable(false);
		txtrKlippetillaeg.setBackground(SystemColor.menu);
		txtrKlippetillaeg.setBounds(338, 89, 99, 23);
		frame.getContentPane().add(txtrKlippetillaeg);
		
		Scrollbar scrollbar = new Scrollbar();
		scrollbar.setBackground(Color.WHITE);
		scrollbar.setBounds(1314, 369, 17, 498);
		frame.getContentPane().add(scrollbar);
		
		Scrollbar scrollbar_1 = new Scrollbar();
		scrollbar_1.setBackground(Color.WHITE);
		scrollbar_1.setOrientation(Scrollbar.HORIZONTAL);
		scrollbar_1.setBounds(0, 850, 1298, 17);
		frame.getContentPane().add(scrollbar_1);
		
		JList list = new JList();
		list.setBounds(10, 153, 1298, 74);
		frame.getContentPane().add(list);
		
		Scrollbar scrollbar_2 = new Scrollbar();
		scrollbar_2.setBackground(Color.WHITE);
		scrollbar_2.setBounds(1314, 157, 17, 76);
		frame.getContentPane().add(scrollbar_2);
		
		JButton btnNewButton_1 = new JButton("\u041E\u0442\u043F\u0440\u0430\u0432\u043A\u0430 \u0434\u0430\u043D\u043D\u044B\u0445 \u043D\u0430 PLC");
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton_1.setHorizontalAlignment(SwingConstants.LEFT);
		btnNewButton_1.setBounds(10, 79, 200, 66);
		frame.getContentPane().add(btnNewButton_1);
	}
}
